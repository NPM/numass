plugins {
    id("space.kscience.gradle.mpp")
    `maven-publish`
}


val dataforgeVersion: String by rootProject.extra

kotlin.sourceSets {
    commonMain {
        dependencies {
            api("space.kscience:dataforge-context:$dataforgeVersion")
            api("space.kscience:dataforge-data:$dataforgeVersion")
            api("org.jetbrains.kotlinx:kotlinx-datetime:${space.kscience.gradle.KScienceVersions.dateTimeVersion}")
        }
    }
    jvmMain{
        dependencies{
            api("ch.qos.logback:logback-classic:1.2.3")
        }
    }
}

kscience{
    useSerialization()
}


