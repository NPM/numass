package ru.inr.mass.data.server

import kotlinx.html.dom.append
import kotlinx.html.js.*
import org.w3c.dom.Element
import space.kscience.dataforge.context.AbstractPlugin
import space.kscience.dataforge.context.Context
import space.kscience.dataforge.context.PluginFactory
import space.kscience.dataforge.context.PluginTag
import space.kscience.dataforge.meta.Meta
import space.kscience.dataforge.names.Name
import space.kscience.dataforge.names.asName
import space.kscience.plotly.models.LineShape
import space.kscience.plotly.models.ScatterMode
import space.kscience.plotly.plotElement
import space.kscience.plotly.scatter
import space.kscience.visionforge.ElementVisionRenderer
import space.kscience.visionforge.Vision
import space.kscience.visionforge.VisionClient
import space.kscience.visionforge.plotly.PlotlyPlugin
import kotlin.reflect.KClass

public class NumassJsPlugin : AbstractPlugin(), ElementVisionRenderer {
    public val client: VisionClient by require(VisionClient)
    public val numassCommon: NumassCommonPlugin by require(NumassCommonPlugin)
    public val plotly: PlotlyPlugin by require(PlotlyPlugin)

    override val tag: PluginTag get() = Companion.tag

    override fun rateVision(vision: Vision): Int = when (vision) {
        is VisionOfNumassHv, is VisionOfNumassPoint, is VisionOfNumassSet -> ElementVisionRenderer.DEFAULT_RATING
        else -> ElementVisionRenderer.ZERO_RATING
    }

    override fun content(target: String): Map<Name, Any> = when (target) {
        ElementVisionRenderer.TYPE -> mapOf("numass".asName() to this)
        else -> super.content(target)
    }

    override fun render(element: Element, vision: Vision, meta: Meta) {
        when (vision) {
            is VisionOfNumassHv -> element.append {
                h1 { +"HV" }
                //TODO add title
                table {
                    th {
                        td { +"Time" }
                        td { +"Value" }
                        td { +"channel" }
                    }
                    vision.forEach { entry ->
                        tr {
                            td { +entry.timestamp.toString() }
                            td { +entry.value.toString() }
                            td { +entry.channel.toString() }
                        }
                    }
                }
            }

            is VisionOfNumassPoint -> element.append {
                h1 { +"Point" }
                plotElement {
                    vision.spectra.forEach { (channel, spectrum) ->
                        val pairs = spectrum.entries.sortedBy { it.key }
                        scatter {
                            name = channel
                            mode = ScatterMode.lines
                            line {
                                shape = LineShape.hv
                            }
                            x.numbers = pairs.map { it.key.toInt() }
                            y.numbers = pairs.map { it.value.toInt() }
                        }
                    }
                }
            }

            is VisionOfNumassSet -> {}
        }
    }


    public companion object : PluginFactory<NumassJsPlugin> {
        override val tag: PluginTag = PluginTag("numass.js", "ru.inr.mass")
        override val type: KClass<NumassJsPlugin> = NumassJsPlugin::class

        override fun build(context: Context, meta: Meta): NumassJsPlugin = NumassJsPlugin()

    }
}