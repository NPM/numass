package ru.inr.mass.data.server

import ru.inr.mass.data.api.NumassPoint
import ru.inr.mass.data.proto.NumassProtoPlugin
import ru.inr.mass.data.proto.readNumassPointFile
import space.kscience.dataforge.context.Context
import space.kscience.dataforge.context.fetch
import java.nio.file.Path


@Suppress("OPT_IN_USAGE")
public suspend fun main() {
    val context = Context("Numass") {
        plugin(NumassProtoPlugin)
        plugin(NumassCommonPlugin)
    }

    val numassProto = context.fetch(NumassProtoPlugin)

    val pointPath = Path.of("D:\\Work\\Numass\\data\\test\\set_7\\p120(30s)(HV1=13300)")
    val point: NumassPoint = numassProto.readNumassPointFile(pointPath)!!

    val visionOfNumass: VisionOfNumassPoint = point.toVision()

    context.makeNumassWebFile {
        vision { visionOfNumass }
    }
}