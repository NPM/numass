package ru.inr.mass.data.server

import space.kscience.dataforge.context.Context
import space.kscience.dataforge.misc.DFExperimental
import space.kscience.visionforge.html.HtmlVisionFragment
import space.kscience.visionforge.html.ResourceLocation
import space.kscience.visionforge.html.VisionPage
import space.kscience.visionforge.html.importScriptHeader
import space.kscience.visionforge.makeFile
import java.awt.Desktop
import java.nio.file.Path


@DFExperimental
public fun Context.makeNumassWebFile(
    path: Path? = null,
    title: String = "VisionForge Numass page",
    resourceLocation: ResourceLocation = ResourceLocation.SYSTEM,
    show: Boolean = true,
    content: HtmlVisionFragment,
): Unit {
    val actualPath = VisionPage(this, content = content).makeFile(path) { actualPath: Path ->
        mapOf(
            "title" to VisionPage.title(title),
            "numassWeb" to VisionPage.importScriptHeader("js/numass-web.js", resourceLocation, actualPath)
        )
    }
    if (show) Desktop.getDesktop().browse(actualPath.toFile().toURI())
}
