package ru.inr.mass.data.server

import io.ktor.http.HttpStatusCode
import io.ktor.http.URLBuilder
import io.ktor.server.application.call
import io.ktor.server.cio.CIO
import io.ktor.server.engine.embeddedServer
import io.ktor.server.response.respondText
import io.ktor.server.routing.get
import io.ktor.server.routing.routing
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ru.inr.mass.data.api.NumassPoint
import ru.inr.mass.data.proto.NumassDirectorySet
import ru.inr.mass.data.proto.NumassProtoPlugin
import ru.inr.mass.data.proto.readRepository
import space.kscience.dataforge.context.Context
import space.kscience.dataforge.context.fetch
import space.kscience.dataforge.data.DataTree
import space.kscience.dataforge.data.await
import space.kscience.dataforge.names.Name
import space.kscience.dataforge.names.NameToken
import space.kscience.dataforge.names.cutLast
import space.kscience.dataforge.names.lastOrNull
import space.kscience.visionforge.server.close
import space.kscience.visionforge.server.openInBrowser
import space.kscience.visionforge.server.visionServer


public suspend fun main() {
    val context = Context("Numass") {
        plugin(NumassProtoPlugin)
        plugin(NumassCommonPlugin)
    }

    val numassProto = context.fetch(NumassProtoPlugin)
    val numassCommon = context.fetch(NumassCommonPlugin)

    val visionManager = numassCommon.visionManager

    val repositroyName = "D:\\Work\\Numass\\data\\test"

    val port = 7777
    val host = "localhost"

    val url = URLBuilder(host = host, port = port).build()

    val repository: DataTree<NumassDirectorySet> = numassProto.readRepository(repositroyName)

    val visionOfNumassRepository = VisionOfNumassRepository(Name.EMPTY, repository)

    val server = context.embeddedServer(CIO, port, host) {
        routing {
            get("/repository") {
                call.respondText {
                    visionManager.encodeToString(visionOfNumassRepository)
                }
            }

            get("/sets/{name...}") {
                val setName: Name? = call.parameters.getAll("name")
                    ?.map { NameToken.parse(it) }?.let(::Name)

                if (setName == null) {
                    call.respondText(status = HttpStatusCode.BadRequest) { "Set name is empty" }
                    return@get
                }

                val set: NumassDirectorySet? = withContext(Dispatchers.IO) { repository[setName]?.await() }
                if (set == null) {
                    call.respondText(status = HttpStatusCode.NotFound) { "A set with name $setName not found in the repository" }
                    return@get
                }

                call.respondText {
                    visionManager.encodeToString(VisionOfNumassSet(setName, set))
                }
            }
            get("/points/{name...}") {
                val fullName: Name? = call.parameters.getAll("name")
                    ?.map { NameToken.parse(it) }?.let(::Name)

                if (fullName == null) {
                    call.respondText(status = HttpStatusCode.BadRequest) { "Point name is empty" }
                    return@get
                }

                val setName = fullName.cutLast()

                val set: NumassDirectorySet? = withContext(Dispatchers.IO) { repository[setName]?.await() }
                if (set == null) {
                    call.respondText(status = HttpStatusCode.NotFound) { "A set with name $setName not found in the repository" }
                    return@get
                }

                val pointIndex: Int? = fullName.lastOrNull()?.body?.toIntOrNull()

                val point: NumassPoint? = set.points.find { it.index == pointIndex }

                if (point == null) {
                    call.respondText(status = HttpStatusCode.NotFound) { "A point with name $setName/$pointIndex not found in the repository" }
                    return@get
                }
                call.respondText {
                    visionManager.encodeToString(point.toVision())
                }
            }
        }
        visionServer(numassCommon.visionManager, url)
    }.start()

//    val server = context.visionManager.serve {
//        header("numass", VisionPage.scriptHeader("js/numass-web.js"))
//        page {
//            div("flex-column") {
//                h1 { +"Visionforge file demo" }
//                vision { visionOfNumass }
//            }
//        }
//    }

    server.openInBrowser()


    println("Enter 'exit' to close server")
    while (readLine() != "exit") {
        //
    }

    server.close()

}