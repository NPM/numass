package ru.inr.mass.data.server

import kotlinx.serialization.modules.SerializersModule
import kotlinx.serialization.modules.polymorphic
import kotlinx.serialization.modules.subclass
import space.kscience.dataforge.context.Context
import space.kscience.dataforge.context.PluginFactory
import space.kscience.dataforge.context.PluginTag
import space.kscience.dataforge.meta.Meta
import space.kscience.visionforge.Vision
import space.kscience.visionforge.VisionPlugin
import kotlin.reflect.KClass

public class NumassCommonPlugin(meta: Meta = Meta.EMPTY) : VisionPlugin(meta) {
    override val tag: PluginTag get() = Companion.tag

    override val visionSerializersModule: SerializersModule get() = numassSerializersModule

    public companion object : PluginFactory<NumassCommonPlugin> {
        override val tag: PluginTag = PluginTag("numass.common", "ru.inr.mass")
        override val type: KClass<NumassCommonPlugin> = NumassCommonPlugin::class

        override fun build(context: Context, meta: Meta): NumassCommonPlugin = NumassCommonPlugin()

        internal val numassSerializersModule = SerializersModule {
            polymorphic(Vision::class) {
                subclass(VisionOfNumassHv.serializer())
                subclass(VisionOfNumassPoint.serializer())
                subclass(VisionOfNumassSet.serializer())
                subclass(VisionOfNumassSetRef.serializer())
                subclass(VisionOfNumassRepository.serializer())
            }
        }
    }
}