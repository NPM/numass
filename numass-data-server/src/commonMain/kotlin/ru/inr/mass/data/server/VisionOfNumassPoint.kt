package ru.inr.mass.data.server

import kotlinx.serialization.Serializable
import ru.inr.mass.data.api.NumassBlock
import ru.inr.mass.data.api.NumassPoint
import ru.inr.mass.data.api.channels
import ru.inr.mass.data.proto.HVData
import ru.inr.mass.data.proto.HVEntry
import space.kscience.dataforge.meta.Meta
import space.kscience.dataforge.names.NameToken
import space.kscience.visionforge.AbstractVision


public typealias SimpleAmplitudeSpectrum = Map<Short, UInt>

internal suspend fun NumassBlock.simpleAmplitudeSpectrum(): SimpleAmplitudeSpectrum {
    val res = mutableMapOf<Short, UInt>()
    events.collect {
        res[it.amplitude] = (res[it.amplitude] ?: 0U) + 1U
    }
    return res
}

@Serializable
public class VisionOfNumassPoint(
    public val pointMeta: Meta,
    public val index: Int,
    public val voltage: Double,
    public val spectra: Map<String, SimpleAmplitudeSpectrum>,
) : AbstractVision()

public suspend fun NumassPoint.toVision(): VisionOfNumassPoint = VisionOfNumassPoint(
    meta,
    index,
    voltage,
    channels.entries.associate { (k, v) ->
        k.toString() to v.simpleAmplitudeSpectrum()
    }
)

@Serializable
public class VisionOfNumassHv(public val hv: HVData) : AbstractVision(), Iterable<HVEntry> {
    override fun iterator(): Iterator<HVEntry> = hv.iterator()
}

private val VisionOfNumassPoint.token: NameToken get() = NameToken("point", index.toString())

