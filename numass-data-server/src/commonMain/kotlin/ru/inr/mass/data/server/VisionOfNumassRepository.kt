package ru.inr.mass.data.server

import kotlinx.serialization.Serializable
import ru.inr.mass.data.api.NumassSet
import space.kscience.dataforge.data.DataTree
import space.kscience.dataforge.data.DataTreeItem
import space.kscience.dataforge.misc.Named
import space.kscience.dataforge.names.Name
import space.kscience.dataforge.names.NameToken
import space.kscience.dataforge.names.plus
import space.kscience.visionforge.AbstractVision
import space.kscience.visionforge.AbstractVisionGroup

@Serializable
public class VisionOfNumassRepository : AbstractVisionGroup() {
    override fun createGroup(): VisionOfNumassRepository = VisionOfNumassRepository()
}

@Serializable
public class VisionOfNumassSetRef(
    override val name: Name,
) : Named, AbstractVision()

public suspend fun VisionOfNumassRepository(
    repoName: Name,
    tree: DataTree<NumassSet>,
): VisionOfNumassRepository = VisionOfNumassRepository().apply {
    tree.items.forEach { (key: NameToken, value) ->
        children[key] = when (value) {
            is DataTreeItem.Leaf -> VisionOfNumassSetRef(repoName + key)
            is DataTreeItem.Node -> VisionOfNumassRepository(repoName + key, value.tree)
        }
    }
    //TODO listen to changes
}