package ru.inr.mass.data.server

import kotlinx.serialization.Serializable
import ru.inr.mass.data.api.NumassSet
import ru.inr.mass.data.proto.HVData
import space.kscience.dataforge.meta.Meta
import space.kscience.dataforge.misc.Named
import space.kscience.dataforge.names.Name
import space.kscience.dataforge.names.plus
import space.kscience.visionforge.AbstractVision

@Serializable
public data class PointRef(
    override val name: Name,
    public val pointMeta: Meta,
    public val index: Int,
    public val voltage: Double,
) : Named

@Serializable
public class VisionOfNumassSet(
    public val meta: Meta,
    public val points: List<PointRef>,
    public val hvData: HVData? = null,
) : AbstractVision()

public fun VisionOfNumassSet(setName: Name, set: NumassSet): VisionOfNumassSet = VisionOfNumassSet(
    set.meta,
    set.points.map { point ->
        PointRef(
            setName + point.index.toString(),
            point.meta,
            point.index,
            point.voltage
        )
    },
    set.hvData
)