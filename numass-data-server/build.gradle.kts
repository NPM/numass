plugins {
    id("space.kscience.gradle.mpp")
    `maven-publish`
}

val visionForgeVersion: String by rootProject.extra

val production: Boolean by rootProject.extra(true)

kotlin {
    js(IR) {
        browser {
            webpackTask {
                this.outputFileName = "js/numass-web.js"
            }
        }
        binaries.executable()
    }

    sourceSets {
        commonMain {
            dependencies {
                implementation(project(":numass-data-model"))
                implementation("space.kscience:visionforge-core:$visionForgeVersion")
                implementation("space.kscience:visionforge-plotly:$visionForgeVersion")
            }
        }
        jvmMain {
            dependencies {
                implementation(project(":numass-data-proto"))
                implementation("space.kscience:visionforge-server:$visionForgeVersion")
            }
        }

    }
}

afterEvaluate {
    val distributionTask = if (production) {
        tasks.getByName("jsBrowserDistribution")
    } else {
        tasks.getByName("jsBrowserDevelopmentExecutableDistribution")
    }

    tasks.getByName<ProcessResources>("jvmProcessResources") {
        dependsOn(distributionTask)
        from(distributionTask)
        include("**/*.js")
        if (production) {
            include("**/*.map")
        }
    }
}

kscience {
    useSerialization {
        json()
    }
    withContextReceivers()
}
