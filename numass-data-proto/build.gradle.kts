plugins {
    id("space.kscience.gradle.jvm")
    id("com.squareup.wire") version "4.4.1"
    `maven-publish`
}

val dataforgeVersion: String by rootProject.extra

dependencies {
    api(project(":numass-data-model"))
    api("space.kscience:dataforge-io:$dataforgeVersion")
}

wire {
    kotlin {
       out = "src/gen/kotlin"
    }
}


sourceSets.main {
    kotlin.srcDir("src/gen/kotlin")
}
