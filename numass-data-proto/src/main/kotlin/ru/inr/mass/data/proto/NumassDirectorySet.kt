package ru.inr.mass.data.proto

import ru.inr.mass.data.api.NumassPoint
import ru.inr.mass.data.api.NumassSet
import ru.inr.mass.data.api.NumassSet.Companion.NUMASS_HV_TARGET
import ru.inr.mass.data.api.readEnvelope
import space.kscience.dataforge.context.*
import space.kscience.dataforge.io.io
import space.kscience.dataforge.io.readEnvelopeFile
import space.kscience.dataforge.meta.Meta
import space.kscience.dataforge.misc.DFExperimental
import space.kscience.dataforge.names.Name
import space.kscience.dataforge.names.asName
import java.nio.file.Files
import java.nio.file.Path
import java.util.stream.Collectors
import kotlin.io.path.*

public class NumassDirectorySet internal constructor(
    public val numassProto: NumassProtoPlugin,
    public val path: Path,
) : NumassSet, ContextAware {

    override val context: Context
        get() = numassProto.context

    @OptIn(DFExperimental::class)
    override val meta: Meta
        get() {
            val metaFilePath = path / "meta"
            return if (metaFilePath.exists()) {
                val envelope = context.io.readEnvelopeFile(metaFilePath)
                envelope.meta
            } else {
                context.logger.warn { "Meta file does not exist for Numass set $metaFilePath" }
                Meta.EMPTY
            }
        }

    override val points: List<NumassPoint>
        get() = Files.list(path).filter {
            it.fileName.name.startsWith("p")
        }.map { pointPath ->
            try {
                numassProto.readNumassPointFile(pointPath)
            } catch (e: Exception) {
                context.logger.error(e) { "Error reading Numass point file $pointPath" }
                null
            }
        }.collect(Collectors.toList()).filterNotNull()


    @OptIn(DFExperimental::class)
    override val hvData: HVData?
        get() = (path / "voltage").takeIf { it.exists() }?.let { hvFile ->
            val envelope = context.io.readEnvelopeFile(hvFile)
            HVData.readEnvelope(envelope)
        }


    override fun content(target: String): Map<Name, Any> = if (target == NUMASS_HV_TARGET) {
        val hvData = hvData
        if (hvData != null) {
            mapOf("hv".asName() to hvData)
        } else {
            emptyMap()
        }
    } else super.content(target)

    public companion object
}
