package ru.inr.mass.data.proto

import ru.inr.mass.data.api.NumassPoint
import space.kscience.dataforge.context.AbstractPlugin
import space.kscience.dataforge.context.Context
import space.kscience.dataforge.context.PluginFactory
import space.kscience.dataforge.context.PluginTag
import space.kscience.dataforge.data.DataSource
import space.kscience.dataforge.data.DataTree
import space.kscience.dataforge.data.static
import space.kscience.dataforge.io.EnvelopeFormatFactory
import space.kscience.dataforge.io.IOPlugin
import space.kscience.dataforge.io.readEnvelopeFile
import space.kscience.dataforge.meta.Meta
import space.kscience.dataforge.misc.DFExperimental
import space.kscience.dataforge.names.Name
import space.kscience.dataforge.names.NameToken
import java.nio.file.Files
import java.nio.file.Path
import kotlin.io.path.exists
import kotlin.io.path.isDirectory
import kotlin.io.path.relativeTo
import kotlin.reflect.KClass

public class NumassProtoPlugin : AbstractPlugin() {
    public val io: IOPlugin by require(IOPlugin)
    override val tag: PluginTag get() = Companion.tag

    override fun content(target: String): Map<Name, Any> = if (target == EnvelopeFormatFactory.ENVELOPE_FORMAT_TYPE) {
        mapOf(TaggedNumassEnvelopeFormat.name to TaggedNumassEnvelopeFormat)
    } else {
        super.content(target)
    }

    public companion object : PluginFactory<NumassProtoPlugin> {
        override fun build(context: Context, meta: Meta): NumassProtoPlugin = NumassProtoPlugin()
        override val tag: PluginTag = PluginTag("numass-proto", group = "ru.inr.mass")
        override val type: KClass<out NumassProtoPlugin> = NumassProtoPlugin::class
    }
}


@OptIn(DFExperimental::class)
public fun NumassProtoPlugin.readNumassPointFile(path: Path): NumassPoint? {
    val envelope = io.readEnvelopeFile(path)
    return ProtoNumassPoint.fromEnvelope(envelope)
}

public fun NumassProtoPlugin.readNumassPointFile(path: String): NumassPoint? = readNumassPointFile(Path.of(path))

public fun NumassProtoPlugin.readNumassDirectory(path: Path): NumassDirectorySet {
    if (!path.exists()) error("Path $path does not exist")
    if (!path.isDirectory()) error("The path $path is not a directory")
    return NumassDirectorySet(this, path)
}

public fun NumassProtoPlugin.readNumassDirectory(path: String): NumassDirectorySet = readNumassDirectory(Path.of(path))
public suspend fun NumassProtoPlugin.readRepository(path: Path): DataTree<NumassDirectorySet> = DataSource {
    Files.walk(path).filter {
        it.isDirectory() && it.resolve("meta").exists()
    }.forEach { childPath ->
        val name = Name(childPath.relativeTo(path).map { segment ->
            NameToken(segment.fileName.toString())
        })
        val value = readNumassDirectory(childPath)
        static(name, value, value.meta)
    }

    //TODO add file watcher
}

public suspend fun NumassProtoPlugin.readRepository(path: String): DataTree<NumassDirectorySet> =
    readRepository(Path.of(path))

public fun NumassProtoPlugin.readPoint(path: String): NumassPoint = readNumassPointFile(path)
    ?: error("Can't read numass point at $path")
