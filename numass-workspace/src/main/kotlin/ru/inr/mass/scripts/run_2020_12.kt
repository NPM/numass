package ru.inr.mass.scripts

import ru.inr.mass.data.proto.NumassDirectorySet
import ru.inr.mass.workspace.Numass.readRepository
import space.kscience.dataforge.data.DataSource
import space.kscience.dataforge.data.DataTree
import space.kscience.dataforge.data.filter
import space.kscience.dataforge.data.forEach
import space.kscience.dataforge.meta.Meta
import space.kscience.dataforge.meta.string

suspend fun main() {
    val repo: DataTree<NumassDirectorySet> = readRepository("D:\\Work\\Numass\\data\\2018_04")
    val filtered: DataSource<NumassDirectorySet> = repo.filter { _, meta: Meta ->
        val operator by meta.string()
        operator?.startsWith("Vas") ?: false
    }

    filtered.forEach{
        println(it)
    }
}