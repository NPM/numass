package ru.inr.mass.workspace

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import ru.inr.mass.data.api.NumassBlock
import ru.inr.mass.data.api.NumassPoint
import ru.inr.mass.data.proto.*
import space.kscience.dataforge.context.fetch
import space.kscience.dataforge.data.DataTree
import space.kscience.dataforge.workspace.Workspace
import java.nio.file.Path

object Numass {
    val workspace = Workspace {
        context {
            plugin(NumassWorkspacePlugin)
        }
    }

    val context get() = workspace.context

    val numassProto by lazy { context.fetch(NumassProtoPlugin) }

    fun readPoint(path: Path): NumassPoint =
        numassProto.readNumassPointFile(path) ?: error("Can't read numass point at $path")

    fun readPoint(path: String): NumassPoint =
        numassProto.readNumassPointFile(path) ?: error("Can't read numass point at $path")

    fun readDirectory(path: Path): NumassDirectorySet = numassProto.readNumassDirectory(path)

    fun readDirectory(path: String): NumassDirectorySet = numassProto.readNumassDirectory(path)

    fun readRepository(path: Path): DataTree<NumassDirectorySet> =
        runBlocking(Dispatchers.IO) { numassProto.readRepository(path) }

    fun readRepository(path: String): DataTree<NumassDirectorySet> =
        runBlocking(Dispatchers.IO) { numassProto.readRepository(path) }

}

fun NumassBlock.listFrames() = runBlocking { frames.toList() }

fun NumassBlock.listEvents() = runBlocking { events.toList() }