package ru.inr.mass.workspace

import ru.inr.mass.data.analysis.NumassAnalyzerParameters
import ru.inr.mass.data.analysis.NumassEventExtractor
import ru.inr.mass.data.analysis.TimeAnalyzer
import ru.inr.mass.data.analysis.analyzeSet
import ru.inr.mass.data.api.NumassSet
import ru.inr.mass.data.proto.NumassProtoPlugin
import space.kscience.dataforge.context.Context
import space.kscience.dataforge.context.PluginFactory
import space.kscience.dataforge.context.PluginTag
import space.kscience.dataforge.data.filterByType
import space.kscience.dataforge.meta.*
import space.kscience.dataforge.meta.descriptors.MetaDescriptor
import space.kscience.dataforge.meta.descriptors.value
import space.kscience.dataforge.names.Name
import space.kscience.dataforge.workspace.WorkspacePlugin
import space.kscience.dataforge.workspace.pipeFrom
import space.kscience.dataforge.workspace.task
import space.kscience.tables.Table
import kotlin.reflect.KClass

class NumassWorkspacePlugin : WorkspacePlugin() {
    override val tag: PluginTag get() = Companion.tag

    val numassProtoPlugin by require(NumassProtoPlugin)

    val selectSets by task<NumassSet>(
        descriptor = MetaDescriptor {
            info = "Select data from workspace data pool"
            value("forward", ValueType.BOOLEAN) {
                info = "Select only forward or only backward sets"
            }
        }
    ) {
        val forward = meta["forward"]?.boolean
        val filtered = workspace.data.filterByType<NumassSet> { _, meta ->
            when (forward) {
                true -> meta["iteration_info.reverse"]?.boolean?.not() ?: false
                false -> meta["iteration_info.reverse"]?.boolean ?: false
                else -> true
            }
        }

        node(Name.EMPTY, filtered)
    }


    val analyzeSets by task<Table<Value>>(
        MetaDescriptor {
            info = "Count the number of events for each voltage and produce a table with the results"
        }
    ) {
        pipeFrom(selectSets) { set, name, meta ->
            val res = TimeAnalyzer(NumassEventExtractor.EVENTS_ONLY).analyzeSet(
                set,
                NumassAnalyzerParameters.read(meta["analyzer"] ?: Meta.EMPTY)
            )
            val outputMeta = meta.toMutableMeta().apply {
                "data" put set.meta
            }
            // context.output.render(res, stage = "numass.analyze", name = name, meta = outputMeta)
            res
        }
    }
//
//    val monitorTableTask: TaskReference<Table<Value>> by task(
//        MetaDescriptor {
//            value("showPlot", type = ValueType.BOOLEAN) {
//                info = "Show plot after complete"
//            }
//            value("monitorPoint", type = ValueType.NUMBER) {
//                info = "The voltage for monitor point"
//            }
//        }
//    ) {
//        val data = from(selectSets)
////        model { meta ->
////            dependsOn(selectTask, meta)
//////        if (meta.getBoolean("monitor.correctForThreshold", false)) {
//////            dependsOn(subThresholdTask, meta, "threshold")
//////        }
////            configure(meta.getMetaOrEmpty("monitor"))
////            configure {
////                meta.useMeta("analyzer") { putNode(it) }
////                setValue("@target", meta.getString("@target", meta.name))
////            }
////        }
//
//        val monitorVoltage = meta["monitorPoint"].double ?: 16000.0
//        val analyzer = TimeAnalyzer()
//        val analyzerMeta = meta["analyzer"]
//
//        //val thresholdCorrection = da
//        //TODO add separator labels
//        val res = ListTable.Builder("timestamp", "count", "cr", "crErr", "index", "set")
//            .rows(
//                data.values.stream().flatMap { set ->
//                    set.points.stream()
//                        .filter { it.voltage == monitorVoltage }
//                        .parallel()
//                        .map { point ->
//                            analyzer.analyzeParent(point, analyzerMeta).edit {
//                                "index" to point.index
//                                "set" to set.name
//                            }
//                        }
//                }
//
//            ).build()
//
//        if (meta["showPlot"].boolean ?: true) {
//            val plot = DataPlot.plot(name, res, Adapters.buildXYAdapter("timestamp", "cr", "crErr"))
//            context.plot(plot, name, "numass.monitor") {
//                "xAxis.title" to "time"
//                "xAxis.type" to "time"
//                "yAxis.title" to "Count rate"
//                "yAxis.units" to "Hz"
//            }
//
//            ((context.output["numass.monitor", name] as? PlotOutput)?.frame as? JFreeChartFrame)?.addSetMarkers(data.values)
//        }
//
//        context.output.render(res, stage = "numass.monitor", name = name, meta = meta)
//
//        data(Name.EMPTY, res)
//    }

    companion object : PluginFactory<NumassWorkspacePlugin> {
        override val tag: PluginTag = PluginTag("numass", "ru.mipt.npm")
        override val type: KClass<out NumassWorkspacePlugin> = NumassWorkspacePlugin::class
        override fun build(context: Context, meta: Meta): NumassWorkspacePlugin = NumassWorkspacePlugin()
    }
}