plugins {
    id("space.kscience.gradle.jvm")
    id("com.github.johnrengelman.shadow") version "7.1.2"
    `maven-publish`
}

kotlin {
    explicitApi = null
}

val dataforgeVersion: String by rootProject.extra
val visionForgeVersion: String by rootProject.extra
val kmathVersion: String by rootProject.extra
val tablesVersion: String by rootProject.extra

dependencies {
    implementation(projects.numassDataProto)
    implementation(projects.numassModel)
    implementation(projects.numassAnalysis)
    implementation("space.kscience:dataforge-workspace:$dataforgeVersion")

    implementation("space.kscience:kmath-jupyter:$kmathVersion")
    implementation("space.kscience:tables-kt:$tablesVersion")
    implementation("space.kscience:visionforge-plotly:$visionForgeVersion")
}

kscience{
    jupyterLibrary("ru.inr.mass.notebook.NumassJupyter")
}