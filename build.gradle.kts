plugins {
    id("space.kscience.gradle.project")
}

allprojects {
    repositories {
        mavenLocal()
        maven("https://repo.kotlin.link")
        maven("https://maven.pkg.jetbrains.space/spc/p/sci/dev")
    }

    group = "ru.inr.mass"
    version = "0.1.3"
}

val dataforgeVersion by extra("0.6.0-dev-15")
val tablesVersion: String by extra("0.2.0-dev-3")
val kmathVersion by extra("0.3.1-dev-6")
val visionForgeVersion: String by rootProject.extra("0.3.0-dev-4")


ksciencePublish{
    space("https://maven.pkg.jetbrains.space/mipt-npm/p/numass/maven")
}

apiValidation {
    validationDisabled = true
}